from damgard_jurik import PublicKey, EncryptedNumber
from damgard_jurik.utils import int_to_mpz
from typing import List
from crypto import keygenN
from damgard_jurik.prime_gen import gen_safe_prime_pair

class MessageQ:
    """Represents encrypted message query result."""

    @int_to_mpz
    def __init__(self, beta: List[List[EncryptedNumber]], pubkeys: List[PublicKey]):
        """Initializes the MessageQ.

        :param beta: The encrypted queries.
        :param pubkeys: The public keys used to encrypt `beta`.
        """
        self.beta = beta
        self.pubkeys = pubkeys

class MessageT:
    """Represents encrypted message transfer result."""

    @int_to_mpz
    def __init__(self, value: int, pubkeys: List[PublicKey]):
        """Initializes the MessageT.

        :param value: The encrypted transfer.
        :param pubkeys: The public keys used to encrypt `value`.
        """
        self.value = value
        self.pubkeys = pubkeys

class CPIR:

    @int_to_mpz
    def __init__(self, base: int, alpha: int):
        """Initializes the CPIR.

        :param base: base of n where n = base^alpha
        :param alpha: log_base(n)
        """
        self.base = base
        self.alpha = alpha

        # generate public keys for each encryption with s_j = s + j
        s = 4
        nbits = 20
        # generating n outside keygen
        p,q = gen_safe_prime_pair(nbits)
        self.pubkeys = []
        self.privkeys = []
        for j in range(self.alpha):
            public_key, private_key_ring = keygenN(
                p,q,
                n_bits=nbits, # key length = 30
                s=s+j,
                threshold=3,
                n_shares=3
            )
            self.pubkeys.append(public_key)
            self.privkeys.append(private_key_ring)

    def query(self, query: List[int]) -> MessageQ:
        """Query Operation.

        :param query: list of index bit to be queried, len(query) = alpha
        """
        assert len(query) == self.alpha
        beta = [] # resulting query encryption
        for j in range(self.alpha):
            beta_j = []
            for t in range(self.base):
                b_jt = 0
                if query[j] == t:
                    b_jt = 1
                beta_jt = self.pubkeys[j].encrypt(b_jt)
                beta_j.append(beta_jt)
            beta.append(beta_j)
        
        return MessageQ(beta, self.pubkeys)

    def transfer(self, msgq: MessageQ, S):
        """Transfer Operation.

        :param msgq: the query message from query operation
        :param S: the private database with length n
        """
        S_j = S
        it = pow(self.base, self.alpha-1)
        for j in range(self.alpha):
            S_j_temp = []
            for i in range(it):
                temp = msgq.beta[j][0]*S_j[i]
                for t in range(1,self.base):
                    temp = temp + msgq.beta[j][t]*S_j[i+it*t]
                S_j_temp.append(temp.value)
            S_j = S_j_temp
            it = it//self.base
        return MessageT(S_j[0], msgq.pubkeys)

    def recover(self, msgt: MessageT):
        S_j = EncryptedNumber(msgt.value, msgt.pubkeys[-1])
        for j in range(self.alpha-1,-1,-1):
            S_j = self.privkeys[j].decrypt(S_j)
            if j > 0:
                S_j = EncryptedNumber(S_j, msgt.pubkeys[j-1])
        return S_j


"""
Power2 Tree CPIR Implementation:
Assumption |S| = n = base^alpha where base = 2^k
"""
# Implementation of Power2 tree but with k = 2
class Power2CPIR:

    @int_to_mpz
    def __init__(self, alpha: int):
        """Initializes the CPIR.

        :param k: base = 2^k and n = base^alpha
        :param alpha: log_base(n)
        """
        self.k = 2
        self.base = pow(2,2)
        self.alpha = alpha

        # generate public keys for each encryption with s_j = s + j
        s = 4
        nbits = 20
        # generating n outside keygen
        p,q = gen_safe_prime_pair(nbits)
        self.pubkeys = []
        self.privkeys = []
        for j in range(self.alpha):
            public_key, private_key_ring = keygenN(
                p,q,
                n_bits=nbits, # key length = 30
                s=s+j,
                threshold=3,
                n_shares=3
            )
            self.pubkeys.append(public_key)
            self.privkeys.append(private_key_ring)

    def query(self, query: List[int]) -> MessageQ:
        """Query Operation.

        :param query: list of index bit to be queried, len(query) = alpha
        """
        assert len(query) == self.alpha*2
        query.reverse()
        beta = [] # resulting query encryption
        for j in range(self.alpha):
            beta_jt = self.pubkeys[j].encrypt(query[2*j])
            beta_j1t = self.pubkeys[j].encrypt(query[2*j+1])
            beta.append([self.pubkeys[j].encrypt(1), beta_jt, beta_j1t, self.pubkeys[j].encrypt(query[2*j+1]*query[2*j])])
        
        return MessageQ(beta, self.pubkeys)

    def transfer(self, msgq: MessageQ, S):
        """Transfer Operation.

        :param msgq: the query message from query operation
        :param S: the private database with length n
        """
        S_j = S
        it = pow(self.base, self.alpha-1)
        for j in range(self.alpha):
            S_j_temp = []
            for i in range(it):
                offset = self.base*i
                temp = msgq.beta[j][0]*S_j[offset] + msgq.beta[j][1]*(S_j[offset+1] - S_j[offset]) \
                    + msgq.beta[j][2]*(S_j[offset+2] - S_j[offset]) + msgq.beta[j][3]*(S_j[offset+3] - S_j[offset+2] - S_j[offset+1] + S_j[offset])
                S_j_temp.append(temp.value)
            S_j = S_j_temp
            it = it//self.base
        return MessageT(S_j[0], msgq.pubkeys)

    def recover(self, msgt: MessageT):
        S_j = EncryptedNumber(msgt.value, msgt.pubkeys[-1])
        for j in range(self.alpha-1,-1,-1):
            S_j = self.privkeys[j].decrypt(S_j)
            if j > 0:
                S_j = EncryptedNumber(S_j, msgt.pubkeys[j-1])
        return S_j


