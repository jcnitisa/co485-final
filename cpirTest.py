from CPIR import *
from random import randint
import pandas as pd
"""
CPIR test for base > 2
"""
if __name__ == "__main__":
    # set general parameter
    baseT = 4
    alphaT = 8 # change if sample size change
    dataset = pd.read_csv("sample16.csv")

    query = []
    index = 0
    for i in range(alphaT):
        query.append(randint(0,baseT-1))
    
    database = dataset["data"].values.tolist()

    it = 1
    for i in range(alphaT-1,-1,-1):
        index += it*query[i]
        it *= baseT

    print("Query:", query)
    print("Index:", index)
    print("Dataset result:", database[index])
    
    cpir = CPIR(baseT, alphaT)
    queryC = cpir.query(query)
    transferC = cpir.transfer(queryC, database)
    print("CPIR result:", cpir.recover(transferC))