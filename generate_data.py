from random import randint
import pandas as pd
import json

sizes = [8, 12, 16, 20]
for size in sizes:
    dataset = []
    for i in range(pow(2,size)):
        dataset.append(randint(0,100000000))
    df = pd.DataFrame(dataset, columns=["data"])
    df.to_csv("sample" + str(size) + ".csv")

