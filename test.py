from CPIR import *
from random import randint
import pandas as pd
import time

if __name__ == "__main__":
    # set general parameter
    baseT = 2
    alphaT = 16 # change if sample size change
    dataset = pd.read_csv("sample16.csv")

    query = []
    index = 0
    for i in range(alphaT):
        query.append(randint(0,baseT-1))
    
    database = dataset["data"].values.tolist()
    it = 1
    for i in range(alphaT-1,-1,-1):
        index += it*query[i]
        it *= baseT

    print("Query:", query)
    print("Index:", index)
    print("Dataset result:", database[index])
    
    # Run the CPIR protocol and compute the time for each process
    cpir = CPIR(baseT, alphaT)
    cpir_start = time.time()

    queryC_start = time.time()
    queryC = cpir.query(query)
    queryC_end = time.time()

    transferC_start = time.time()
    transferC = cpir.transfer(queryC, database)
    transferC_end = time.time()

    print("CPIR result:", cpir.recover(transferC))
    cpir_end = time.time()


    # Run the P2CPIR protocol and compute the time for each process
    P2cpir = Power2CPIR(alphaT//2)
    P2cpir_start = time.time()

    queryP_start = time.time()
    queryP = P2cpir.query(query)
    queryP_end = time.time()

    transferP_start = time.time()
    transferP = P2cpir.transfer(queryP, database)
    transferP_end = time.time()

    print("P2CPIR result:", P2cpir.recover(transferP), "\n")
    P2cpir_end = time.time()

    print("Protocol Time:")
    print("CPIR Total time:", cpir_end - cpir_start)
    print("CPIR Query time:", queryC_end - queryC_start)
    print("CPIR Transfer time:", transferC_end - transferC_start, "\n")
    print("P2CPIR Total time:", P2cpir_end - P2cpir_start)
    print("P2CPIR Query time:", queryP_end - queryP_start)
    print("P2CPIR Transfer time:", transferP_end - transferP_start)