# CO485 Final
Implementation of Lip05 LFCPIR with log-squared communication complexity and P2CPIR which is the optimized CPIR protocol.


## Getting started
For the initialization, run:
```
git clone https://git.uwaterloo.ca/jcnitisa/co485-final.git
cd co485-final
pip install -r requirements.txt
```
The libraries included are:
1. Damgard-Jurik: Implementation of DJ01 cryptosystem, used as the base for the Log-squared CPIR protocol by Lip05 as it satisfy the additively homomorphism criteria.
2. Pandas: Scripti to generate dataset for the cryptosystem input.

## Files Description

- CPIR.py: Implementation of the CPIR protocol
- crypto.py: Flexible version of keygen from Damgard-Jurik to allow passing primes p,q for the secret key
- test.py: Example for running both LFCPIR and P2CPIR for same database and query, including the running time.
- cpirTest.py: Example for running LFCPIR with any value of lambda

To see the example result, run
```
python test.py
```
