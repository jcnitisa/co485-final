from math import factorial
from typing import Tuple

from damgard_jurik import PrivateKeyShare, PublicKey, PrivateKeyRing
from damgard_jurik.shamir import share_secret
from damgard_jurik.utils import crm

"""
    Damgard-Jurik Cryptosystem implementation from damgard-jurik lib
    Simplified version for KeygenN with addition to pass the value of n
"""

def keygenN(p: int,
            q: int,
            n_bits: int = 64,
           s: int = 1,
           threshold: int = 3,
           n_shares: int = 3) -> Tuple[PublicKey, PrivateKeyRing]:
    """Generates a PublicKey and a PrivateKeyRing using the threshold variant of Damgard-Jurik.
    with the prime pair as input to fix the value of n

    :param p: The prime with property (p-1)/2 also prime.
    :param q: The prime with property (q-1)/2 also prime.
    :param n_bits: The number of bits to use in the public and private keys.
    :param s: The power to which n = p * q will be raised. Plaintexts live in Z_n^s.
    :param threshold: The minimum number of PrivateKeyShares needed to decrypt an encrypted number.
    :param n_shares: The number of PrivateKeyShares to generate.
    :return: A tuple containing the generated PublicKey and PrivateKeyRing.
    """
    # Ensure valid parameters
    if n_bits < 16:  # Note: 16 is somewhat arbitrary
        raise ValueError('Minimum number of bits for encryption is 16')

    if s < 1:
        raise ValueError('s must be greater than 1')

    if n_shares < threshold:
        raise ValueError('The number of shares must be at least as large as the threshold')

    if threshold < 1:
        raise ValueError('The threshold and number of shares must be at least 1')

    # Find n = p * q and m = p_prime * q_prime where p = 2 * p_prime + 1 and q = 2 * q_prime + 1
    p_prime, q_prime = (p - 1) // 2, (q - 1) // 2
    n, m = p * q, p_prime * q_prime

    # Pre-compute for convenience
    n_s = n ** s
    n_s_m = n_s * m

    # Find d such that d = 0 mod m and d = 1 mod n^s
    d = crm(a_list=[0, 1], n_list=[m, n_s])

    # Use Shamir secret sharing to share_secret d
    shares = share_secret(
        secret=d,
        modulus=n_s_m,
        threshold=threshold,
        n_shares=n_shares
    )

    # Create PublicKey and PrivateKeyShares
    delta = factorial(n_shares)
    public_key = PublicKey(n=n, s=s, m=m, threshold=threshold, delta=delta)
    private_key_shares = [PrivateKeyShare(public_key=public_key, i=i, s_i=s_i) for i, s_i in shares]
    private_key_ring = PrivateKeyRing(private_key_shares=private_key_shares)

    return public_key, private_key_ring
